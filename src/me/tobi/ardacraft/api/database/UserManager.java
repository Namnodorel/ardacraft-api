package me.tobi.ardacraft.api.database;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserManager {

    DatabaseConnection conn;

    public UserManager(DatabaseConnection connection) {
        conn = connection;
    }

    public boolean contains(String uuid) {
        return get(uuid) != null;
    }

    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM USERS;");
            while(result.next()) {
                users.add(
                        new User(result.getString("UUID"),
                                ArdaCraftAPI.getACDatabase().getCharacterManager().get(result.getInt("CHARACTER_ID")),
                        result.getString("LAST_NAME"), result.getInt("CHARACTER_CHANGE_DATE"), result.getInt("LAST_SEEN"))
                );
            }
            result.close();
            statement.close();
        }catch(SQLException ex) {ex.printStackTrace();}
        return users;
    }

    public User get(String uuid) {
        String lastName = "";
        Charakter charakter = null;
        long dateCharacterChanged = 0;
        long lastSeen = 0;

        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM USERS WHERE UUID='" + uuid + "';");
            if(result.next()) {
                lastName = result.getString("LAST_NAME");
                charakter = ArdaCraftAPI.getACDatabase().getCharacterManager().get(result.getInt("CHARACTER_ID"));
                dateCharacterChanged = result.getInt("CHARACTER_CHANGE_DATE");
                lastSeen = result.getInt("LAST_SEEN");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return new User(uuid, charakter, lastName, dateCharacterChanged, lastSeen);
    }

    public User getByName(String name) {
        String uuid = "";
        String lastName = "";
        Charakter charakter = null;
        long dateCharacterChanged = 0;
        long lastSeen = 0;

        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM USERS WHERE LAST_NAME='" + name + "';");
            if(result.next()) {
                uuid = result.getString("UUID");
                lastName = result.getString("LAST_NAME");
                charakter = ArdaCraftAPI.getACDatabase().getCharacterManager().get(result.getInt("CHARACTER_ID"));
                dateCharacterChanged = result.getInt("CHARACTER_CHANGE_DATE");
                lastSeen = result.getInt("LAST_SEEN");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return new User(uuid, charakter, lastName, dateCharacterChanged, lastSeen);
    }

    public void delete(String uuid) {
        conn.execute("DELETE FROM USERS WHERE UUID='" + uuid + "';");
    }

    public void update(User user) {
        String query = "UPDATE USERS SET ";
        if(user.getLastName() != null) {
            query += "LAST_NAME='" + user.getLastName() + "', ";
        }
        if(user.getCharakter() != null) {
            query += "CHARACTER_ID='" + user.getCharakter().getId() + "', ";
        }
        if(user.getDateCharacterChanged() > 10) {
            query += "CHARACTER_CHANGE_DATE='" + user.getDateCharacterChanged() + "', ";
        }
        if(user.getLastSeen() > 10) {
            query += "LAST_SEEN='" + user.getLastSeen() + "' ";
        }
        if(query.length() > 20) {
            query += "WHERE UUID='" + user.getUUID() + "';";
            conn.execute(query);
        }
    }

    public void add(User user) {
        if(!contains(user.getUUID())) {
            if(user.getCharakter() != null) {
                conn.execute("INSERT INTO USERS (UUID,LAST_NAME,CHARACTER_ID,CHARACTER_CHANGE_DATE, LAST_SEEN) VALUES ('" +
                        user.getUUID() + "', '" +
                        user.getLastName() + "', " +
                        user.getCharakter().getId() + ", " +
                        user.getDateCharacterChanged() + ", " +
                        user.getLastSeen() + ");");
            }else {
                conn.execute("INSERT INTO USERS (UUID,LAST_NAME,CHARACTER_ID,CHARACTER_CHANGE_DATE, LAST_SEEN) VALUES ('" +
                        user.getUUID() + "', '" +
                        user.getLastName() + "', " +
                        null + ", " +
                        user.getDateCharacterChanged() + ", " +
                        user.getLastSeen() + ");");
            }
        }else {
            System.err.println("User already exists!");
        }
    }

}