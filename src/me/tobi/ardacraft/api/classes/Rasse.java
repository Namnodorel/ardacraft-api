package me.tobi.ardacraft.api.classes;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.PotionEffectManager;
import me.tobi.ardacraft.api.classes.Utils.Attitude;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public enum Rasse {
	
	ORK{
		@Override
		public String getName() {
			return "Ork";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Kampfaxt [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, NAZGUL{
		@Override
		public String getName() {
			return "Nazgul";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Dolch [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, URUKHAI{
		@Override
		public String getName() {
			return "Urukhai";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Armbrust [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, TROLL{
		@Override
		public String getName() {
			return "Troll";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));
			rtn.add(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Krummschwert [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, WARGREITER{
		@Override
		public String getName() {
			return "Wargreiter";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Elbenbogen [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, OSTLING{
		@Override
		public String getName() {
			return "Ostling";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Menschenschwert [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.BAD;};
	}, ELB{
		@Override
		public String getName() {
			return "Elb";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Elbenbogen [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, MENSCH{
		@Override
		public String getName() {
			return "Mensch";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Menschenschwert [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, MAGIER{
		@Override
		public String getName() {
			return "Magier";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 4));
			rtn.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0));
			return rtn; 
		}
		@Override
		public String getAbility() {
			return "Magierstab [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, ZWERG{
		@Override
		public String getName() {
			return "Zwerg";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Kampfaxt [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, HOBBIT{
		@Override
		public String getName() {
			return "Hobbit";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.HUNGER, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Dolch [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, ENT{
		@Override
		public String getName() {
			return "Ent";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Krummschwert [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, DUNEDAIN{
		@Override
		public String getName() {
			return "Dunedain";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> rtn = new ArrayList<PotionEffect>();
			rtn.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0));
			rtn.add(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0));
			return rtn;
		}
		@Override
		public String getAbility() {
			return "Armbust [Item]";
		}
		@Override public Attitude getAttitude(){return Attitude.GOOD;};
	}, UNREGISTERED{
		@Override
		public String getName() {
			return "Unregistriert";
		}
		@Override
		public List<PotionEffect> getEffects() {
			List<PotionEffect> effects = new ArrayList<PotionEffect>();
			return effects;
		}
		@Override
		public String getAbility() {
			return "Nicht vorhanden!";
		}
		@Override public Attitude getAttitude(){return Attitude.UNKNOWN;};
	}, ALLE_RASSEN;
	
	/**
	 * 
	 * @return Die Liste der Potion Effekte
	 */
	public List<PotionEffect> getEffects() {
		List<PotionEffect> effects = new ArrayList<PotionEffect>();
		return effects;
	}
	
	/**
	 * 
	 * @return Den Namen der Rasse
	 */
	public String getName() {
		return null;
	}
	
	/**
	 * 
	 * @return Die Gesinnung der Rasse
	 */
	public Attitude getAttitude() {
		return Attitude.UNKNOWN;
	}
	/**
	 * 
	 * @return Die F�higkeiten der Rasse
	 */
	public String getAbility() {
		return null;
	}
	/**
	 * Gibt dem Spieler <b>p</b> seine Rassenspeziefischen Effekte
	 * @param p Player
	 */
	public static void applyEffects(Player p) {
		for(PotionEffect pe : get(p).getEffects()) {
			PotionEffectManager.addPotionEffect(p, pe);
		}
		/*for(PotionEffect pe : get(p).getEffects()) { //für alle VölkerEffekte
			if(p.hasPotionEffect(pe.getType())) { //falls der spieler bereits einen potioneffect dieser sorte hat
				for(PotionEffect ppe : p.getActivePotionEffects()) { //für alle aktiven effekte
					if(ppe.getType() == pe.getType()) { //für den effekte, den der spieler bereits hat
						if(ppe.getAmplifier() < pe.getAmplifier()) { //falls der amplifier kleiner als der vom spieler ist
							p.addPotionEffect(pe);
						}
					}
				}
			}else {
				p.addPotionEffect(pe);				
			}
		}*/
	}
	
	/**
	 * 
	 * @param p Player
	 * @return Die Rasse des Spieler <b>p</b>
	 */
	public static Rasse get(Player p) {
		Charakter c = Charakter.get(p);
		if(c == null) {
			return Rasse.UNREGISTERED;
		}else {
			return c.getRasse();
		}
		
	}
}