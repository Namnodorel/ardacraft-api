package me.tobi.ardacraft.api.classes;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Region {

    private int id;
    private String name;
    private City city;
    private Location location;
    private int cpState;

    private Region() {}

    public static Region construct(int id, City city, String name, Location loc, int cpState) {
        Region r = new Region();
        r.setId(id);
        r.setName(name);
        r.setCity(city);
        r.setLocation(loc);
        r.setCpState(cpState);
        return r;
    }

    public static Region create(City city, String name, Location location) {
        Region r = new Region();
        r.setId(-1);
        r.setName(name);
        r.setCity(city);
        r.setLocation(location);
        r.setCpState(0);
        ArdaCraftAPI.getACDatabase().getRegionManager().add(r);
        Region added = ArdaCraftAPI.getACDatabase().getRegionManager().get(name);
        Location minLoc = location.getBlock().getLocation().add(-50, -1*location.getBlockY(), -50);
        BlockVector min = new BlockVector(minLoc.getBlockX(), minLoc.getBlockY(), minLoc.getBlockZ());
        Location maxLoc = location.getBlock().getLocation().add(50, -1*location.getBlockY(), 50);
        BlockVector max = new BlockVector(maxLoc.getBlockX(), maxLoc.getBlockY(), maxLoc.getBlockZ());
        ProtectedRegion region = new ProtectedCuboidRegion("ACREGION_" + city.getId() + "-" + added.getId(), min, max);
        region.setFlag(DefaultFlag.BLOCK_BREAK, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.BUILD, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.CHEST_ACCESS, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.DAMAGE_ANIMALS, StateFlag.State.DENY);

        WGBukkit.getPlugin().getRegionManager(Bukkit.getWorld("RPG")).addRegion(region);
        return r;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getCpState() {
        return cpState;
    }

    public void setCpState(int cpState) {
        this.cpState = cpState;
    }

    public ProtectedRegion getWorldGuardRegion() {
        return WGBukkit.getPlugin().getRegionManager(Bukkit.getWorld("RPG")).getRegion("ACREGION_" + id);
    }

}
