package me.tobi.ardacraft.api.classes;

import me.tobi.ardacraft.api.classes.Logger.Action;
import me.tobi.ardacraft.api.classes.Logger.Level;

import org.bukkit.Location;

public class LogEntry {
	
	public LogEntry(long time, Level level, Action action, String causer, String message, Location location) {
		this.setTime(time);
		this.setLevel(level);
		this.setAction(action);
		this.setCauser(causer);
		this.setMessage(message);
		this.setLocation(location);
	}
	
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public String getCauser() {
		return causer;
	}

	public void setCauser(String causer) {
		this.causer = causer;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	private long time;
	private Level level;
	private Action action;
	private String causer;
	private String message;
	private Location location;
	
	
}