package me.tobi.ardacraft.api.classes;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;

public class City {

	private int id;
	private String name;
	private User owner;
	private Location location;
	private boolean isGood;
	private HashMap<User, Utils.Position> userPositions;
	
	private City() {}

    public static City construct(int id, String name, User owner, Location loc, boolean isGood, HashMap<User, Utils.Position> userPositions) {
        City c = new City();
        c.setId(id);
        c.setName(name);
        c.setOwner(owner);
        c.setLocation(loc);
        c.setGood(isGood);
        c.setUsers(userPositions);
        return c;
    }

	public static City create(String name, User owner, Location loc, boolean isGood, HashMap<User, Utils.Position> userPositions) {
		City c = new City();
		c.setId(-1);
		c.setName(name);
		c.setOwner(owner);
		c.setLocation(loc);
		c.setGood(isGood);
		c.setUsers(userPositions);
        ArdaCraftAPI.getACDatabase().getCityManager().add(c);
        Region.create(c, "main", loc);
		return c;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public boolean isGood() {
		return isGood;
	}

	public void setGood(boolean good) {
		isGood = good;
	}

	public HashMap<User, Utils.Position> getUsers() {
		return userPositions;
	}

	public void setUsers(HashMap<User, Utils.Position> userPositions) {
		this.userPositions = userPositions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public void addRegion(Region region) {

    }


	public static String getDatabaseName(String name) {
		name = name.replaceAll(" ", "_");
		name = name.toLowerCase();
		return name;
	}

	public static String getDisplayName(String name) {
		char[] chars = name.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		boolean toUpp = false;
		for(int i = 0; i < name.length(); i++) {
			if(toUpp) {
				chars[i] = Character.toUpperCase(chars[i]);
				toUpp = false;
			}
			if(chars[i] == '_') {
				chars[i] = ' ';
				toUpp = true;
			}
		}
		return String.valueOf(chars);
	}

	public List<Region> getRegions() {
		return ArdaCraftAPI.getACDatabase().getRegionManager().get(this);
	}
	
}
